package com.xu.springcloud.service;

import com.xu.springcloud.entity.Dept;

import java.util.List;

public interface IDeptService {
    public boolean addDept(Dept dept);

    public Dept queryById(Long id);

    public List<Dept> queryAll();
}
