package com.xu.springcloud.Controller;

import com.xu.springcloud.entity.Dept;
import com.xu.springcloud.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Dept")
public class DeptController1 {
    @Autowired
    IDeptService iDeptService;
    @Autowired
    DiscoveryClient client;
    @PostMapping("/add")
    public Boolean add(Dept dept){
        return iDeptService.addDept(dept);
    }
    @GetMapping("/get/{id}")
    public Dept get(@PathVariable("id") Long id){
        return iDeptService.queryById(id);
    }
    @GetMapping("/list")
    public List<Dept> get(){
        return iDeptService.queryAll();
    }
    @GetMapping("/discovery")
    public Object discovery(){
        //获取微服务列表的清单
        List<String> services = client.getServices();
        //得到一个具体的微服务信息,通过具体的微服务id，applicaioinName；
        List<ServiceInstance> instances = client.getInstances("SPRINGCLOUD-PROVIDER-DEPT");
        System.out.println("discovery=>services:"+services);
        for (ServiceInstance instance : instances) {
            System.out.println(
                    instance.getHost()+"\t"+
                            instance.getPort()+"\t"+
                            instance.getUri()+"\t"+
                            instance.getServiceId()

            );
        }
        return this.client;
    }
}
