package com.xu.springcloud.controller;

import com.xu.springcloud.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * 消费部门
 */
@RestController
public class DeptConsumerController {

    @Autowired
    RestTemplate restTemplate;
    //这里的REST_URL_PREFIX设置为服务提供者的id，方便消费者从注册中心调用
    private static final String REST_URL_PREFIX = "http://SPRINGCLOUD-PROVIDER-DEPT";
    @PostMapping("/consumer/add")
    public Boolean add(Dept dept){
        return restTemplate.postForObject(REST_URL_PREFIX+"/Dept/add",dept,Boolean.class);
    }
    @GetMapping("/consumer/get/{id}")
    public Dept get(@PathVariable("id") Long id){
        return restTemplate.getForObject(REST_URL_PREFIX+"/Dept/get/"+id,Dept.class);
    }
    @GetMapping("/consumer/list")
    public List<Dept> list(){
        return restTemplate.getForObject(REST_URL_PREFIX+"/Dept/list",List.class);
    }
}
