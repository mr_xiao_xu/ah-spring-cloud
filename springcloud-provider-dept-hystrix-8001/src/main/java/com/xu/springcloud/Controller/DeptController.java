package com.xu.springcloud.Controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.xu.springcloud.entity.Dept;
import com.xu.springcloud.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Dept")
public class DeptController {
    @Autowired
    IDeptService iDeptService;
    @PostMapping("/add")
    public Boolean add(Dept dept){
        return iDeptService.addDept(dept);
    }
    //服务熔断 fallbackMethod =新的方法名
    @HystrixCommand(fallbackMethod = "hystrixGet")
    @GetMapping("/get/{id}")
    public Dept get(@PathVariable("id") Long id){
        Dept dept = iDeptService.queryById(id);
        if (dept==null){
          throw  new RuntimeException("报错");
        }
        return dept;
    }

    public Dept hystrixGet(@PathVariable("id") Long id){
        Dept dept = new Dept();
        dept.setDeptno(id);
        dept.setDname("阿崔提醒您-->您本次输入的部门号不存在");
        dept.setDbSource("您好！我是阿崔！-->没有这个数据库");
        return dept;
    }
}
