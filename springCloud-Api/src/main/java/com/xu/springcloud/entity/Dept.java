package com.xu.springcloud.entity;


import lombok.experimental.Accessors;

import java.io.Serializable;
@Accessors(chain = true)//链试结构
public class Dept implements Serializable {

  private long deptno;
  private String dname;
  private String dbSource;


  public long getDeptno() {
    return deptno;
  }

  public void setDeptno(long deptno) {
    this.deptno = deptno;
  }


  public String getDname() {
    return dname;
  }

  public void setDname(String dname) {
    this.dname = dname;
  }


  public String getDbSource() {
    return dbSource;
  }

  public void setDbSource(String dbSource) {
    this.dbSource = dbSource;
  }

  public Dept(String dname) {
    this.dname = dname;
  }

  public Dept(long deptno, String dname, String dbSource) {
    this.deptno = deptno;
    this.dname = dname;
    this.dbSource = dbSource;
  }

  public Dept() {
  }
}
